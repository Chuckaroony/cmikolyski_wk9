package Hibernate;

import java.util.Scanner;

public class CreateAccount {
    public void getInput() {
        Scanner theScanner = new Scanner(System.in);

        boolean run = true;
        String userName = null;
        String userPassword = null;
        while (run) {
            System.out.println("Please enter a user name and a password. Character length must be 20 or less" +
                    "and user name must be one word. Enter quit to exit.");
            userName = theScanner.nextLine();
            if(userName.equalsIgnoreCase("quit")) {
                System.out.println("Good bye!");
                System.exit(0);
            }

            userPassword = theScanner.nextLine();
            if (InputValidation.ValidateInput(userName, userPassword) && !userName.equalsIgnoreCase("quit"))  {
                if(!InputValidation.checkMatchCreateAccount(userName)) {
                    DataAccessObjects dbInstance = DataAccessObjects.getInstance();
                    Player newPlayer = new Player(userName, userPassword);
                    dbInstance.insertPlayer(newPlayer);
                    run = false;
                }
            }



            else{
                System.out.println("That user name already exists.");
            }
        }
    }

}
