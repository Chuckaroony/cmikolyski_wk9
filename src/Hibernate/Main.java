package Hibernate;

import java.util.Scanner;


public class Main {

    public static void selectAction(){
        Scanner myScanner = new Scanner(System.in);
        boolean run = true;
        String userInput;

        while(run) {
            System.out.println("Enter 1 to login or 2 to create a new account." +
                    "Enter quit to exit.");
            userInput = myScanner.nextLine();
            if (userInput.equalsIgnoreCase("1") ){
                Login newLogin = new Login();
                newLogin.getInput();
                run = false;
            }

            else if (userInput.equalsIgnoreCase("2")) {
                CreateAccount newAccount = new CreateAccount();
                newAccount.getInput();
                run=false;
            }

            else if (userInput.equalsIgnoreCase("quit")) {
                System.out.println("Good bye!");
                System.exit(0);
            }
        }
    }



    public static void main(String[] args) {

        DataAccessObjects dbInstance = DataAccessObjects.getInstance();
        selectAction();

    }
}
