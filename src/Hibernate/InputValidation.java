package Hibernate;

import javax.xml.crypto.Data;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputValidation {

    public static boolean ValidateInput (String userInput, String userPassword) {
        int maxLength = 20;
        Pattern pattern = Pattern.compile("[ ]");
        if ((userInput != null || userInput.length() <= maxLength) && (userPassword != null || userPassword
        .length() <= maxLength)) {
            Matcher matcher = pattern.matcher(userInput);
            boolean matchFound = matcher.find();
            if (matchFound) {
                return false;
            }
        }
        else {
            return false;
        }
        return true;
    }

    public static boolean checkMatchLogin(String name, String password){
        DataAccessObjects dbInstance = DataAccessObjects.getInstance();
        List<Player> players = dbInstance.getPlayers();
        for (Player i : players) {
            if (i.getName().equals(name) && i.getPassword().equals(password) ){
                return true;
            }
        }

        return false;
    }

    public static boolean checkMatchCreateAccount(String name){
        DataAccessObjects dbInstance = DataAccessObjects.getInstance();
        List<Player> players = dbInstance.getPlayers();
        for (Player i : players) {
            if (i.getName().equals(name)){
                return true;
            }
        }
        return false;
    }
}
