package Hibernate;

import java.util.Scanner;

public class Login {

    public void getInput() {
        Scanner theScanner = new Scanner(System.in);

        boolean run = true;
        String userName = null;
        String userPassword = null;
        while (run) {
            System.out.println("Please enter your user name followed by your password. Enter quit to exit.");
            userName = theScanner.nextLine();
            if(userName.equalsIgnoreCase("quit")) {
                System.out.println("Good bye!");
                System.exit(0);
            }

            userPassword = theScanner.nextLine();
            if (InputValidation.ValidateInput(userName, userPassword) && !userName.equalsIgnoreCase("quit"))  {
                if(InputValidation.checkMatchLogin(userName, userPassword)) {
                    System.out.println("Welcome back " + userName + "!");
                    run = false;
                }
            }

            else{
                System.out.println("User name and password are incorrect or account does not exist.");
            }
        }
    }

}
