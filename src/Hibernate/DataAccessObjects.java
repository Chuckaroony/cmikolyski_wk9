package Hibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.Iterator;
import java.util.List;

public class DataAccessObjects {

    SessionFactory factory = null;
    Session session = null;

    private static DataAccessObjects single_instance = null;

    private DataAccessObjects()
    {
        factory = HibernateUtilities.getSessionFactory();
    }

    public static DataAccessObjects getInstance() {
        if (single_instance == null) {
            single_instance = new DataAccessObjects();
        }

        return single_instance;
    }


    public List<Player> getPlayers() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Player";
            List<Player> queryList = (List<Player>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return queryList;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Unable to perform request. Unable to contact " +
                    "database or something is wrong with the query.");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Player getPlayer(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Player where idplayer=" + Integer.toString(id);
            Player thePlayer = (Player) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return thePlayer;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("There is a problem with the query.");

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public void insertPlayer(Player newPlayer) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(newPlayer);
            session.getTransaction().commit();
            System.out.println("Adding new player was a success.");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("There is a problem adding new player.");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

}
